﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;

public class PlayerController : MonoBehaviour
{
    public Camera cam;
    public NavMeshAgent navAgent;
    public ThirdPersonCharacter character;

    void Start()
    {
        //navAgent.updateRotation = false;
        //cam = Camera.main;
        //navAgent = this.GetComponent<NavMeshAgent>();//GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray rayCam = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit rayHit;

            if (Physics.Raycast(rayCam, out rayHit))
            {
                navAgent.SetDestination(rayHit.point);
            }
        }
        if (navAgent.remainingDistance > navAgent.stoppingDistance)
        {
            character.Move(navAgent.desiredVelocity, false, false);
            Debug.Log("Desired Velocity " + navAgent.desiredVelocity + " - " + navAgent.remainingDistance);
        }
        else
        {
            character.Move(Vector3.zero, false, false);
            Debug.Log("Something wrong here !!! " + navAgent.remainingDistance);
        }
    }

}
