Learning NavMesh from Brackeys's tutorial. 
You can clone origin asset from here: https://github.com/Brackeys/NavMesh-Tutorial

Brackeys's video about NavMesh which currently i'm learning.
- Unity NavMesh Tutorial - Basics : https://www.youtube.com/watch?v=CHV1ymlw-P8
- Unity NavMesh Tutorial - Making it Dynamic : https://www.youtube.com/watch?v=FkLJ45Pt-mY
- Unity NavMesh Tutorial - Animated Character : https://www.youtube.com/watch?v=blPglabGueM